// Copyright (c) Philipp Wagner. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

package com.fidocredit.app.camera.mockup;

import android.hardware.Camera;
import android.util.Log;

public class CameraErrorCallback implements Camera.ErrorCallback {

    private static final String TAG = "FaceDetectionMockup";

    @Override
    public void onError(int error, Camera camera) {
        Log.e(TAG, "Encountered an unexpected camera error: " + error);
    }
}
// Copyright (c) Philipp Wagner. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

package com.fidocredit.app.camera.mockup;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class CameraActivity extends Activity
        implements SurfaceHolder.Callback {

    public final String TAG = "FaceDetectionMockup";

    private Camera mCamera;
    private int mCameraId;

    // We need the phone orientation to correctly draw the overlay:
    private int mOrientation;
    private int mOrientationCompensation;
    private OrientationEventListener mOrientationEventListener;

    // Let's keep track of the display rotation and orientation also:
    private int mDisplayRotation;
    private int mDisplayOrientation;

    // Holds the Face Detection result:
    private Camera.Face[] mFaces;

    // The surface view for the camera data
    private SurfaceView mView;

    // Draw rectangles and other fancy stuff:
    private FaceOverlayView mFaceView;

    // Log all errors:
    private final CameraErrorCallback mErrorCallback = new CameraErrorCallback();

    /**
     * Sets the faces for the overlay view, so it can be updated
     * and the face overlays will be drawn again.
     */
    private FaceDetectionListener faceDetectionListener = new FaceDetectionListener() {
        @Override
        public void onFaceDetection(Face[] faces, Camera camera) {
            Log.d(TAG, "Number of Faces : " + faces.length);
            // Update the view now!
            mFaceView.setFaces(faces);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView = new SurfaceView(this);

        setContentView(mView);
        // Now create the OverlayView:
        mFaceView = new FaceOverlayView(this);
        addContentView(mFaceView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        View control = getLayoutInflater().inflate(R.layout.control, null);
        View button = control.findViewById(R.id.takepicture);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.takePicture(null, null, mPicture);
            }
        });
        addContentView(control, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        // Create and Start the OrientationListener:
        mOrientationEventListener = new SimpleOrientationEventListener(this);
        mOrientationEventListener.enable();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        SurfaceHolder holder = mView.getHolder();
        holder.addCallback(this);
    }

    @Override
    protected void onPause() {
        mOrientationEventListener.disable();
        super.onPause();
    }

    @Override
    protected void onResume() {
        mOrientationEventListener.enable();
        super.onResume();
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        mCameraId = findOptimalCamera();
        mCamera = Camera.open(mCameraId);

        // start face detection only *after* preview has started
        Camera.Parameters params = mCamera.getParameters();
        if (params.getMaxNumDetectedFaces() > 0){
            // camera supports face detection, so can start it:
            mCamera.setFaceDetectionListener(faceDetectionListener);
            try {
                mCamera.setPreviewDisplay(surfaceHolder);
            } catch (Exception e) {
                Log.e(TAG, "Could not preview the image !!", e);
                Toast.makeText(this, "FCould not preview the image !!", Toast.LENGTH_LONG).show();
            }
            mCamera.startPreview();
            mCamera.startFaceDetection();
            Toast.makeText(this, "Face detection started", Toast.LENGTH_LONG).show();
        } else {
            Log.e(TAG, "Camera doesnt support face detection !!");
            Toast.makeText(this, "Camera doesnt support face detection !!", Toast.LENGTH_LONG).show();
        }

        try {
            mCamera.setPreviewDisplay(surfaceHolder);
        } catch (Exception e) {
            Log.e(TAG, "Could not preview the image.", e);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        // We have no surface, return immediately:
        if (surfaceHolder.getSurface() == null) {
            return;
        }
        // Try to stop the current preview:
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // Ignore...
        }

        configureCamera(width, height);
        setDisplayOrientation();
        setErrorCallback();

        // Everything is configured! Finally start the camera preview again:
        mCamera.startPreview();
    }

    private void setErrorCallback() {
        mCamera.setErrorCallback(mErrorCallback);
    }

    private void setDisplayOrientation() {
        // Now set the display orientation:
        mDisplayRotation = Util.getDisplayRotation(CameraActivity.this);
        mDisplayOrientation = Util.getDisplayOrientation(mDisplayRotation, mCameraId);

        mCamera.setDisplayOrientation(mDisplayOrientation);

        if (mFaceView != null) {
            mFaceView.setDisplayOrientation(mDisplayOrientation);
        }
    }


    private int findOptimalCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            cameraId = i;
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                break;
            }
        }
        return cameraId;
    }

    private void configureCamera(int width, int height) {
        Camera.Parameters params = mCamera.getParameters();
        // Set the PreviewSize :
        setOptimalPreviewSize(params, width, height);
        // And set the parameters:
        List<String> focusModes = params.getSupportedFlashModes();
        if (focusModes != null) {
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                params.setFlashMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
        }

        Util.setAppropriateCameraPictureSize(mCamera, mView, params);

        params.setRotation(mDisplayOrientation);
        mCamera.setParameters(params);
    }

    private void setOptimalPreviewSize(Camera.Parameters cameraParameters, int width, int height) {
        List<Camera.Size> previewSizes = cameraParameters.getSupportedPreviewSizes();
        float targetRatio = (float) width / height;
        Camera.Size previewSize = Util.getOptimalPreviewSize(this, previewSizes, targetRatio);
        cameraParameters.setPreviewSize(previewSize.width, previewSize.height);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        mCamera.setPreviewCallback(null);
        mCamera.setFaceDetectionListener(null);
        mCamera.setErrorCallback(null);
        mCamera.release();
        mCamera = null;
    }

    /**
     * We need to react on OrientationEvents to rotate the screen and
     * update the views.
     */
    private class SimpleOrientationEventListener extends OrientationEventListener {

        public SimpleOrientationEventListener(Context context) {
            super(context, SensorManager.SENSOR_DELAY_NORMAL);
        }

        @Override
        public void onOrientationChanged(int orientation) {
            Log.d(TAG, "orientation changed");

            // We keep the last known orientation. So if the user first orient
            // the camera then point the camera to floor or sky, we still have
            // the correct orientation.
            if (orientation == ORIENTATION_UNKNOWN) return;

            mOrientation = Util.roundOrientation(orientation, mOrientation);
            // When the screen is unlocked, display rotation may change. Always
            // calculate the up-to-date orientationCompensation.
            int orientationCompensation = mOrientation + Util.getDisplayRotation(CameraActivity.this);
            if (mOrientationCompensation != orientationCompensation) {
                mOrientationCompensation = orientationCompensation;
                mFaceView.setOrientation(mOrientationCompensation);
            }
        }
    }

    private File getDir() {
        File sdDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, TAG);
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFileDir = getDir();
            if (pictureFileDir == null) {
                Log.e(TAG, "Error creating media file, check storage permissions !!");
                Toast.makeText(getApplicationContext(), "Error creating media file, check storage permissions !!", Toast.LENGTH_LONG).show();
                return;
            }

            if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                Log.e(TAG, "Can't create directory to save image !!");
                Toast.makeText(getApplicationContext(), "Can't create directory to save image !!", Toast.LENGTH_LONG).show();
                return;
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
            String date = dateFormat.format(new Date());

            String photoFile = "selfie_" + date + "_rotated_";
            String filename = pictureFileDir.getPath() + File.separator + photoFile;

            Log.d(TAG, "[mDisplayRotation, mDisplayOrientation, mOrientation, mOrientationCompensation]");
            Log.d(TAG, "[" + mDisplayRotation + ", "  + mDisplayOrientation + ", " + mOrientation + ", " + mOrientationCompensation + "]");

            saveRotatedImage(filename + "0.jpeg", 0, data, camera);
            saveRotatedImage(filename + "90.jpeg", 90, data, camera);
            saveRotatedImage(filename + "-90.jpeg", -90, data, camera);
            saveRotatedImage(filename + "180.jpeg", 180, data, camera);
            saveRotatedImage(filename + "270.jpeg", 270, data, camera);
        }

        private void saveRotatedImage(String rotatedFileName, int degree, byte[] data, Camera camera) {
            try {
                // convert byte array into bitmap
                Bitmap loadedImage = null;

                loadedImage = BitmapFactory.decodeByteArray(data, 0, data.length);

                // rotate Image
                Matrix rotateMatrix = new Matrix();
                rotateMatrix.postRotate(degree);
//                Util.prepareMatrix(rotateMatrix, true , degree, camera.getParameters().getPictureSize().width, camera.getParameters().getPictureSize().height);
                loadedImage = Bitmap.createBitmap(loadedImage, 0, 0, loadedImage.getWidth(), loadedImage.getHeight(), rotateMatrix, false);

                ByteArrayOutputStream ostream = new ByteArrayOutputStream();

                // save image into gallery
                loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, ostream);

                File pictureFile = new File(rotatedFileName);
                FileOutputStream fout = new FileOutputStream(pictureFile);
                fout.write(ostream.toByteArray());
                fout.close();

                Log.i(TAG, "New Rotated Image saved to :" + rotatedFileName + ", image size : " + pictureFile.length()/1024);
                Toast.makeText(getApplicationContext(), "New Rotated Image saved to :" + rotatedFileName + ", image size : " + pictureFile.length()/1024, Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Image could not be saved !! " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    };

}
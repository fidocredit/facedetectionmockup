// Copyright (c) Philipp Wagner. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

package com.fidocredit.app.camera.mockup;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.hardware.Camera.Face;
import android.util.Log;
import android.view.View;

/**
 * This class is a simple View to display the faces.
 */
public class FaceOverlayView extends View {

    private Paint mPaint;
    private Paint mTextPaint;
    private int mDisplayOrientation;
    private int mOrientation;
    private Face[] mFaces;

    private static final String TAG = "FaceDetectionMockup";

    public FaceOverlayView(Context context) {
        super(context);
        initialize();
    }

    private void initialize() {
        // We want a green box around the face:
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.WHITE);
        mPaint.setAlpha(255);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(20);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setDither(true);
        mTextPaint.setTextSize(20 * getResources().getDisplayMetrics().density);
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setStyle(Paint.Style.FILL);
    }

    public void setFaces(Face[] faces) {
        mFaces = faces;
        invalidate();
    }

    public void setOrientation(int orientation) {
        mOrientation = orientation;
    }

    public void setDisplayOrientation(int displayOrientation) {
        mDisplayOrientation = displayOrientation;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mFaces != null && mFaces.length > 0) {
            Log.d(TAG, "redrawing faces : " + mFaces.length);
            Matrix matrix = new Matrix();
            Util.prepareMatrix(matrix, true, mDisplayOrientation, getWidth(), getHeight());
            canvas.save();
//            canvas.rotate(mOrientation);
            RectF rectF = new RectF();
            String text;
            for (Face face : mFaces) {
                rectF.set(face.rect);
                Log.d(TAG, "face rect : " + rectF);
                matrix.mapRect(rectF);
                rectF.set(rectF.left - (int) (0.15 * rectF.width()), rectF.top - (int) (0.25 * rectF.height()), rectF.right + (int) (0.15 * rectF.width()), rectF.bottom + (int) (0.25 * rectF.height()));
                Log.d(TAG, "face rotated rect : " + rectF);
                canvas.drawOval(rectF, mPaint);

                if (face.score < 50)
                    text = "Not good enough";
                else if (face.score < 80)
                    text = "Good enough";
                else
                    text = "Very good";

                canvas.drawText("Ratio " + (int)(100*(face.rect.height() * face.rect.width()) / (canvas.getHeight() * canvas.getWidth())), rectF.right, rectF.bottom, mTextPaint);
                canvas.drawText(text, rectF.right, rectF.top, mTextPaint);
//                canvas.drawText("" + face.rect.height() * face.rect.width(), rectF.left, rectF.top, mTextPaint);
//                canvas.drawText("" + canvas.getHeight() * canvas.getWidth(), rectF.left, rectF.bottom, mTextPaint);
            }
            canvas.restore();
        } else {
            Log.w(TAG, "No Face Detected");
        }
    }
}